package com.skblab.registration;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.scheduling.TaskScheduler;
import org.springframework.scheduling.concurrent.ThreadPoolTaskScheduler;

@SpringBootApplication
public class RegistrationApplication {

    public static void main(String[] args) {
        SpringApplication.run(RegistrationApplication.class, args);
    }

    @Bean
    public TaskScheduler notifierRetryScheduler() {
        final ThreadPoolTaskScheduler notifierRetryScheduler = new ThreadPoolTaskScheduler();
        notifierRetryScheduler.setPoolSize(5);
        notifierRetryScheduler.setThreadNamePrefix("notifierRetryScheduler");
        notifierRetryScheduler.setWaitForTasksToCompleteOnShutdown(true);
        notifierRetryScheduler.setAwaitTerminationSeconds(30);
        return notifierRetryScheduler;
    }
}
