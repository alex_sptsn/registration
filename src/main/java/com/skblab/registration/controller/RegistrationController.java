package com.skblab.registration.controller;

import com.skblab.registration.controller.dto.UserRegistrationDTO;
import com.skblab.registration.service.UserService;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PostMapping;

import javax.validation.Valid;

@Controller
public class RegistrationController {

    private final UserService userService;

    public RegistrationController(UserService userService) {
        this.userService = userService;
    }

    @PostMapping("/registration")
    public String registration(@Valid UserRegistrationDTO userRegistration) {
        userService.register(userRegistration.toUser());
        return "redirect:/success.html";
    }
}
