package com.skblab.registration.controller.dto;

import com.skblab.registration.domain.User;

import javax.validation.constraints.NotBlank;

public class UserRegistrationDTO {

    @NotBlank
    private String login;
    @NotBlank
    private String pass;
    @NotBlank
    private String email;
    @NotBlank
    private String firstName;
    @NotBlank
    private String lastName;
    private String patronymic;

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getPass() {
        return pass;
    }

    public void setPass(String pass) {
        this.pass = pass;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getPatronymic() {
        return patronymic;
    }

    public void setPatronymic(String patronymic) {
        this.patronymic = patronymic;
    }

    public User toUser() {
        return User.builder()
                .setLogin(login)
                .setPass(pass)
                .setEmail(email)
                .setFirstName(firstName)
                .setLastName(lastName)
                .setPatronymic(patronymic)
                .build();
    }
}
