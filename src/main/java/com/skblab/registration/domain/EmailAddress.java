package com.skblab.registration.domain;

public final class EmailAddress {

    private final String address;

    public EmailAddress(String address) {
        this.address = address;
    }

    public static EmailAddress of(String email) {
        return new EmailAddress(email);
    }
}
