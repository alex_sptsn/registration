package com.skblab.registration.domain;

public final class EmailContent {

    private final Object content;

    public EmailContent(Object content) {
        this.content = content;
    }
}
