package com.skblab.registration.domain;

public enum RegistrationApprovalResult {
    queued, approved, rejected
}
