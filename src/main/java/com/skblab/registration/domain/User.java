package com.skblab.registration.domain;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.util.Objects;

@Entity
public final class User {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private long id;
    @NotBlank
    private String login;
    //TODO хранить хэш пароля
    @NotBlank
    private String pass;
    @NotBlank
    private String email;
    @NotBlank
    private String firstName;
    @NotBlank
    private String lastName;
    private String patronymic;
    @NotNull
    @Enumerated(EnumType.ORDINAL)
    @Column(name = "registration_approval_result", columnDefinition = "integer default 0")
    private RegistrationApprovalResult registrationApprovalResult;

    protected User() {
    }

    public static Builder builder() {
        return new Builder();
    }

    public long getId() {
        return id;
    }

    public String getLogin() {
        return login;
    }

    public String getPass() {
        return pass;
    }

    public String getEmail() {
        return email;
    }

    public String getFirstName() {
        return firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public String getPatronymic() {
        return patronymic;
    }

    public RegistrationApprovalResult getRegistrationApprovalResult() {
        return registrationApprovalResult;
    }

    public static final class Builder {
        private User delegate = new User();

        public Builder setId(long id) {
            delegate.id = id;
            return this;
        }

        public Builder setLogin(String login) {
            delegate.login = login;
            return this;
        }

        public Builder setPass(String pass) {
            delegate.pass = pass;
            return this;
        }

        public Builder setEmail(String email) {
            delegate.email = email;
            return this;
        }

        public Builder setFirstName(String firstName) {
            delegate.firstName = firstName;
            return this;
        }

        public Builder setLastName(String lastName) {
            delegate.lastName = lastName;
            return this;
        }

        public Builder setPatronymic(String patronymic) {
            delegate.patronymic = patronymic;
            return this;
        }

        public Builder setRegistrationApprovalResult(RegistrationApprovalResult registrationApprovalResult) {
            delegate.registrationApprovalResult = registrationApprovalResult;
            return this;
        }

        public User build() {
            Objects.requireNonNull(delegate, "builder already closed");
            final User result = delegate;
            delegate = null;
            return result;
        }
    }
}
