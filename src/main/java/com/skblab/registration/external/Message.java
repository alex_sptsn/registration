package com.skblab.registration.external;

public final class Message<T> {

    private final T content;

    public Message(T content) {
        this.content = content;
    }

    public T getContent() {
        return content;
    }
}
