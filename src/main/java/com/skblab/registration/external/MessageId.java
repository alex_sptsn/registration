package com.skblab.registration.external;

import java.util.Objects;
import java.util.UUID;

public final class MessageId {

    private final UUID uuid;

    public MessageId(UUID uuid) {
        this.uuid = uuid;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        MessageId messageId = (MessageId) o;
        return uuid.equals(messageId.uuid);
    }

    @Override
    public int hashCode() {
        return Objects.hash(uuid);
    }
}
