package com.skblab.registration.external;

import com.skblab.registration.domain.RegistrationApprovalResult;

public class UserRegistrationApprovalResult {
    private final long userId;
    private final RegistrationApprovalResult approvalResult;

    public UserRegistrationApprovalResult(long userId, RegistrationApprovalResult approvalResult) {
        this.userId = userId;
        this.approvalResult = approvalResult;
    }

    public long getUserId() {
        return userId;
    }

    public RegistrationApprovalResult getApprovalResult() {
        return approvalResult;
    }
}
