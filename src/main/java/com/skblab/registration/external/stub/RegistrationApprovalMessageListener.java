package com.skblab.registration.external.stub;

import com.skblab.registration.external.Message;
import com.skblab.registration.external.MessageListener;
import com.skblab.registration.external.UserRegistrationApprovalResult;
import com.skblab.registration.service.UserService;
import org.springframework.stereotype.Component;

/**
 * Листенер сообщений (например, Rabbit) из внешней системы, из которой приходят сообщения об одобрении регистрации
 */
@Component
public class RegistrationApprovalMessageListener implements MessageListener<UserRegistrationApprovalResult> {

    private final UserService userService;

    public RegistrationApprovalMessageListener(UserService userService) {
        this.userService = userService;
    }

    @Override
    public void handleMessage(Message<UserRegistrationApprovalResult> incomingMessage) {
        final UserRegistrationApprovalResult messageContent = incomingMessage.getContent();
        userService.completeRegistration(messageContent.getUserId(), messageContent.getApprovalResult());
    }
}
