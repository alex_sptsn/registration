package com.skblab.registration.repository;

import com.skblab.registration.domain.RegistrationApprovalResult;
import com.skblab.registration.domain.User;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

public interface UserRepository extends CrudRepository<User, Long> {

    @Modifying
    @Query("update User set registrationApprovalResult = :registrationApprovalResult where id = :userId")
    int updateRegistrationApprovalResultForUser(@Param("userId") long userId,
                                                @Param("registrationApprovalResult") RegistrationApprovalResult registrationApprovalResult);
}
