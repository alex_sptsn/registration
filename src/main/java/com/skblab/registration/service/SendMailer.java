package com.skblab.registration.service;

import com.skblab.registration.domain.EmailAddress;
import com.skblab.registration.domain.EmailContent;

import java.util.concurrent.TimeoutException;

/**
 * Ориентировочный интерфейс мейлера.
 */
public interface SendMailer {
    void sendMail(EmailAddress toAddress, EmailContent messageBody) throws TimeoutException;
}
