package com.skblab.registration.service;

import com.skblab.registration.domain.User;

public interface UserNotifierService {

    void notify(User user, String message);
}
