package com.skblab.registration.service;

import com.skblab.registration.domain.RegistrationApprovalResult;
import com.skblab.registration.domain.User;

public interface UserService {

    void register(User user);

    void completeRegistration(long userId, RegistrationApprovalResult registrationApprovalResult);

    class RegistrationException extends RuntimeException {
        public RegistrationException(String message) {
            super(message);
        }
    }

    class UserNotFoundException extends RuntimeException {
        public UserNotFoundException(String message) {
            super(message);
        }
    }
}
