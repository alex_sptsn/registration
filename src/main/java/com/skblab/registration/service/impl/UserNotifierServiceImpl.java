package com.skblab.registration.service.impl;

import com.skblab.registration.domain.EmailAddress;
import com.skblab.registration.domain.EmailContent;
import com.skblab.registration.domain.User;
import com.skblab.registration.service.SendMailer;
import com.skblab.registration.service.UserNotifierService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.scheduling.TaskScheduler;
import org.springframework.stereotype.Service;

import java.util.concurrent.TimeoutException;

@Service
public class UserNotifierServiceImpl implements UserNotifierService {

    private static final Logger log = LoggerFactory.getLogger(UserNotifierServiceImpl.class);

    private static final int maxTries = 3;

    private final SendMailer sendMailer;
    private final TaskScheduler notifierRetryScheduler;

    public UserNotifierServiceImpl(SendMailer sendMailer, @Qualifier("notifierRetryScheduler") TaskScheduler notifierRetryScheduler) {
        this.sendMailer = sendMailer;
        this.notifierRetryScheduler = notifierRetryScheduler;
    }

    @Override
    public void notify(User user, String message) {
        notifyWithRetry(user, message, 0, 0);
    }

    private void notifyWithRetry(User user, String message, int tryCount, long delay) {
        try {
            sendMailer.sendMail(EmailAddress.of(user.getEmail()), new EmailContent(message));
        } catch (TimeoutException e) {
            log.error("Can't send email to user " + user.getId(), e);

            final int tryNumber = tryCount + 1;
            if (tryNumber > maxTries) {
                throw new RuntimeException("Retries number exceeded", e);
            }
            final long newDelay = (delay + 1000) * 2;
            notifierRetryScheduler.scheduleWithFixedDelay(
                    () -> notifyWithRetry(user, message, tryNumber, newDelay),
                    newDelay
            );
        }
    }
}
