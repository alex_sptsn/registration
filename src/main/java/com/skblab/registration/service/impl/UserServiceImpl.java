package com.skblab.registration.service.impl;

import com.skblab.registration.domain.RegistrationApprovalResult;
import com.skblab.registration.domain.User;
import com.skblab.registration.external.Message;
import com.skblab.registration.repository.UserRepository;
import com.skblab.registration.service.MessagingService;
import com.skblab.registration.service.UserNotifierService;
import com.skblab.registration.service.UserService;
import org.springframework.stereotype.Service;

import java.util.Objects;

@Service
public class UserServiceImpl implements UserService {

    private final UserRepository userRepository;
    private final MessagingService messagingService;
    private final UserNotifierService userNotifierService;

    public UserServiceImpl(UserRepository userRepository, MessagingService messagingService, UserNotifierService userNotifierService) {
        this.userRepository = userRepository;
        this.messagingService = messagingService;
        this.userNotifierService = userNotifierService;
    }

    @Override
    public void register(User user) {
        Objects.requireNonNull(user, "user required");

        final User savedUser = userRepository.save(user);
        messagingService.send(new Message<>(savedUser));
    }

    @Override
    public void completeRegistration(long userId, RegistrationApprovalResult registrationApprovalResult) {
        Objects.requireNonNull(registrationApprovalResult, "registrationApprovalResult required");

        final boolean updated = userRepository.updateRegistrationApprovalResultForUser(userId, registrationApprovalResult) == 1;
        if (!updated) {
            throw new RegistrationException("registrationApprovalResult wasn't updated for user with id " + userId);
        }

        final User user = userRepository.findById(userId)
                .orElseThrow(() -> new UserNotFoundException("User not found by id " + userId));
        userNotifierService.notify(user, "Registration approved");
    }
}
